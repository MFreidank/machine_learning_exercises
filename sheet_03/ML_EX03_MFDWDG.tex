\documentclass[11pt,a4paper]{../exercisesheet}
 
% ------------------------- packages ------------------------------------------
% encoding and language
\usepackage[utf8]{inputenc}
\usepackage[english,ngerman]{babel}
\usepackage{comment}
\usepackage{marvosym}
\usepackage{graphics}
\usepackage{float}
\usepackage{esdiff}

% font
\usepackage[T1]{fontenc}
\usepackage{lmodern}
 \usepackage{color}
\usepackage{neuralnetwork}
% hyperlink (URL, etc.)
\usepackage{hyperref}
\hypersetup{	 colorlinks = true,	 
		 urlcolor = red!70!black,
		 linkcolor = red!70!black,
		 }
% citation
\usepackage{cite}
 
% math
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{nicefrac}

% code
\usepackage{listings}
 
% pseudocode
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{algorithmicx}
 
% graphics
\usepackage{graphicx}
\usepackage{tikz}                 % includes xcolor
\usetikzlibrary{arrows}
\usetikzlibrary{shapes}
%------------------------------------------------------------------------------
 
 
% ------------------------ listings code style settings -----------------------
% define colors
\definecolor{colorNavy}{HTML}{00CC33}
\definecolor{dkgreen}{RGB}{0,100,0}
 
% code style
\lstset{ %
  %linewidth=\textwidth,
  %linewidth=16cm,
  language=JAVA,                  % the language of the code
  %basicstyle=\footnotesize,      % the size of the fonts that are used for the code
  numbers=left,                   % where to put the line-numbers
  stepnumber=2,                   % the step between two line-numbers. If it's 1, each line 
                                  % will be numbered
  numbersep=5pt,                  % how far the line-numbers are from the code
  frame=single,                   % adds a frame around the code
  rulecolor=\color{black},        % if not set, the frame-color may be changed on line-breaks
  tabsize=2,                      % sets default tabsize to 2 spaces
  captionpos=b,                   % sets the caption-position to bottom
  breaklines=true,                % sets automatic line breaking
  breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
  %keywordstyle=\color{blue},      % keyword style
  %commentstyle=\color{dkgreen},   % comment style
  %stringstyle=\color{colorNavy},  % string literal style
  morekeywords={*,with, where, from, union, all, as},
  extendedchars=true,
  literate={ä}{{\"{a}}}1 {ö}{{\"o}}1 {ü}{{\"u}}1,
}
%------------------------------------------------------------------------------
 
 
% ------------------------- commands ------------------------------------------
\newcommand{\todo}[1]{{\em \color{blue}[#1]}\marginpar{{\bf [!!!]}} }
%------------------------------------------------------------------------------
 
 
% ------------------------ exercisesheet class design settings ----------------
\colorlet{maincolor}{red!70!black}
 
\let\sffamily=\rmfamily
 
\setsheetfont{sheet title}{\sffamily\bfseries\Huge}
\setsheetfont{sheet topic}{\sffamily\bfseries\Huge\color{maincolor}}
\setsheetfont{exercise label}{\sffamily\bfseries\color{maincolor}}
%------------------------------------------------------------------------------
 
 
% ------------------------- exercisesheet class text settings -----------------
\setsheettemplate{sheet title (student)}{Exercise Sheet No.~\thesheet}
 
\sheetconf{
    lecture   = Machine Learning,
    lecturer  =  Albert-Ludwigs Universität Freiburg,
    semester  = Summer Term 2015,
    author    = {Moritz Freidank 3339768, Dominik Winterer 3330181, Dev Ganatra 3759726}
}
%------------------------------------------------------------------------------
\setsheettemplate{credits name}{points}
\makeatletter
\newcommand*{\rom}[1]{\expandafter\@slowromancap\romannumeral #1@}
\makeatother
\begin{document}
\selectlanguage{english}
\sheet[
    number= 3,
    deadline = Due:~\today,
    topic={Gradient Descent\\
    Forward-Backward Pass\\
    Multi-Layer Perceptrons\\}
]

\exercise[topic = {Gradient Descent}, label={}]
In this exercise, we want to find a local minimum of the function 
\begin{equation*}
f(u) = \frac{u^3}{3} + 50 u^2 -100 u -30\\
\end{equation*}
The gradient of f with respect to $u$ is 
\begin{equation*}
\mathit{gradf}(u) = \frac{\partial}{\partial u}\ f(u)   = u^2 + 100 u -100
\end{equation*}
\subsection*{(a) \href{http://ml.informatik.uni-freiburg.de/_media/documents/teaching/ss15/05_mlps.print.pdf\#page=23}{With Momentum:}}
Parameters: $\epsilon = 0.01$, $\mu = 0.1$
\begin{itemize}
 \item Initialization: $u = 20, \Delta = 0$
 \item 1. Iteration: $\Delta = -23$, $u = -3$
 \item 2. Iteration: $\Delta = -23$, $u = -3$ %TODO: THIS SEEMS BROKEN
 \item 3. Iteration: $\Delta \approx 2.53$, $u \approx 1.14$
 \item 4. Iteration: $\Delta \approx 0.09$, $u \approx 1.24$
\end{itemize}
Reported Minimum: $\boxed{u = 1.24}$\\

\newpage
\subsection*{(b) \href{http://ml.informatik.uni-freiburg.de/_media/documents/teaching/ss15/05_mlps.print.pdf\#page=19}{Vanilla Gradient Descent}}

\underline{Remark:} Since $\mu = 0$ for vanilla gradient descent, it is sufficient to update $u$, ignoring $\Delta$ entirely. 
Thus, we do not state $\Delta$ explicitly below.\\ \\
Parameters: $\epsilon = 0.01$, $\mu = 0$

\begin{itemize}
 \item Initialization: $u = 20, \Delta = 0$
 \item 1. Iteration: $u = -3$
 \item 2. Iteration: $u = 0.91$
 \item 3. Iteration: $u \approx 0.991$
 \item 4. Iteration: $u \approx 0.990$
\end{itemize}
Reported Minimum: $\boxed{u = 0.99}$\\ \\

\subsection*{Benefit Evaluation}
In the given setting, both vanilla gradient descent and gradient descent with momentum converge towards the same local minimum.\\
Explicit calculation of this minimum shows that the true value of this local minimum of $f(u)$ is:\\
\begin{equation*}
 u_{\mathit{min}} = -50 + 10 \sqrt{26} \approx 0.990195
\end{equation*}
From this we can conclude, that vanilla gradient descent converges faster towards the correct minimum than gradient descent with momentum. 
To verify this conclusion we ran the experiment in simulation and found that it takes $11$ iterations for gradient descent with momentum to approximate $u_{\mathit{min}}$ with comparable precision. 
Furthermore, we discovered that this is not merely a case of a badly chosen parameter value for $\mu$ since the performance of gradient descent with momentum appears to be worse than vanilla gradient descent 
for all values of $\mu$(including negative values) unless $\mu \approx 0$.\\ \\
\exercise[topic = {Forward-Backward-Pass}, label={}]
\subsection*{(a)}
Activations and Output of the MLP:\\
\begin{equation*}
a_1 = \frac{1}{1+e^{-1}} \approx 0.731
\end{equation*}

\begin{equation*}
a_2 = \frac{1}{1+e^{2-0.731}} \approx \boxed{0.22 = y}
\end{equation*}\\
Target Value: $c(x) = 0.5$


\subsection*{(b)}

\exercise[topic = {Multi-Layer Perceptron Architecture}, label={}]
\subsection*{(a)}
We consider the MLP as a directed acyclic graph. 
Then, input neurons are sources of the graph and output neurons are sinks of the graph.\\
This yields the following result:\\ \\
Input Neurons: $\{F,I\}$\\
Output Neurons: $\{E\}$
\subsection*{(b)}
\begin{itemize}
 \item Input Layer: $\{F, I\}$
 \item 1. Hidden Layer: $\{B, A, H\}$
 \item 2. Hidden Layer: $\{C,D\}$
 \item 3. Hidden Layer: $\{J,G\}$
 \item Output Layer: $\{E\}$
\end{itemize}
The MLP has a total of five layers.
\subsection*{(c)}
Activations for neurons in the same layer can be calculated during the same timesteps.
Activations for neurons in a lower layer must be calculated before the activations for neurons in a higher layer can be calculated. 
Thus, the listing of the layers in (b) gives the correct order.
\exercise[topic = {Multi-Layer Perceptrons}, label={}]

\end{document}