#!/usr/bin/env python

"""
Python source code - replace this with a description of the code and write the code below this text.
"""

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import numpy as np

class RandomBooleanFormula(object):
    """docstring for RandomBooleanFormula"""
    def __init__(self, noVariables, noLiterals):
        super(RandomBooleanFormula, self).__init__()
        self.noVariables = noVariables
        self.noLiterals = noLiterals
        self.formula =""

        self.setFormula()
    def setFormula(self):
        variables_nonegation = ["~"+ chr(ord('A')+i) for i in range(0, self.noVariables)]
        variables_negation = ["~"+ chr(ord('A')+i) for i in range(0, self.noVariables)]

        junktor = "+ " 
        noTerms = int(self.noLiterals/3)

        for i in range(0, noTerms):
            term = "("
            for j in range(0, len(variables_nonegation)):
                offset = 1
                var = ""
                ran = np.random.uniform(0,1,1)
                index = int(np.random.uniform(0, len(variables_negation)))
                ran2 = np.random.uniform(0,1,1)
                pick_var = True
                if ran2 < 0.2:
                    pick_var = False
                if(term == "(" or pick_var):
                    if ran >= 0.5:
                        while variables_nonegation[index] in term or variables_negation[index] in term:
                            if index > 0:
                                index -= 1
                            else:
                                    index += offset
                                    offset += 1
                        var = variables_nonegation[index]
                    else:
                        while variables_nonegation[index] in term or variables_negation[index]in term:
                            if index > 0:
                                index -= 1
                            else:
                                    index += offset
                                    offset += 1
                        var = variables_negation[index]
                    if j < len(variables_nonegation)-1:
                        term += var + " & "
                    else:
                        term += var + ")"
            if i > 0:
                self.formula += junktor
            self.formula += term

    def printFormula(self):
        """TODO: Docstring for printFormula.
        :returns: TODO

        """
        print(self.formula)

