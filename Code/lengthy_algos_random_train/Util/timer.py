#!/usr/bin/env python

"""
Python source code - replace this with a description of the code and write the code below this text.
"""

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import datetime as dt
class Timer(object):
    """docstring for Timer"""
    def __init__(self):
        super(Timer, self).__init__()
        self.start_time = dt.datetime.now().replace(microsecond = 0)

    def getExerciseDuration(self):
        end_time = dt.datetime.now().replace(microsecond = 0)
        print(end_time - self.start_time)

    def checkExerciseDuration(self, filename):
        end_time = dt.datetime.now().replace(microsecond = 0)
        current = (end_time - self.start_time)
        f = open(filename, 'r')
        previous_best = f.read()
        f.close()
        if previous_best != "":
            t = dt.datetime.strptime(previous_best,"%H:%M:%S")
            delta = dt.timedelta(hours=t.hour, minutes=t.minute, seconds=t.second)
            if delta > current:
                print("improved!")
                print("New best time:")
                print(current)
                f = open(filename, 'w')
                f.write(str(current))
                f.close()
            else:
                print("no improvement over old best time:") 
                print(delta)
        else:
            f = open(filename, 'w')
            f.write(str(current))
            f.close()
