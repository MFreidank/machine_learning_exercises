#!/usr/bin/env python

"""
Allows generating random 2d (linearly seperable or not) datasets
"""

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import numpy as np
import sys

def generateDataSet(noAttributes, noPoints):
    points = []
    a = {}
    for i in range(0, noAttributes):
        name = "A_"+str(i)
        noAttributeVals = int(np.random.uniform(2, 5, 1))
        a.update({name : ["v_"+str(i)+"_" + str(j) for j in range(1, noAttributeVals)]})
    for i in range(0, noPoints):
        points.append([])
        for key in a.keys():
            valueIndex = int(np.random.uniform(0, len(a[key])))
            points[i].append(a[key][valueIndex])
    return points

def generateTwoClassDataSet(noAttributes, noPoints):
    """TODO: Docstring for generateDataSet.

    :returns: TODO

    """
    points = generateDataSet(noAttributes, noPoints)
    classes = []
    for i in range(0, len(points)):
        coin = np.random.uniform(0,1,1)
        if coin >= 0.5:
            classes.append(1)
        else:
            classes.append(0)

    return [points, classes]
    



def printSet(points, classes):
    assert(len(points) == len(classes))
    tabs = "\t"
    for i in range(0,len(points[0])):
        tabs+="\t"
    print("Features" + tabs+"Class")
    for j in range(0, len(points)):
        print(points[j], end = "\t\t")
        print(classes[j])
    
