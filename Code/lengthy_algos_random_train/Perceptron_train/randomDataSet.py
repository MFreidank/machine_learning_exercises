#!/usr/bin/env python

"""
Allows generating random 2d (linearly seperable or not) datasets
"""

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import numpy as np
import sys
from perceptron import *

def getClassification(points, line):
    """Method to get the true classification of the points

    :points: list of all points to classify
    :line: list containing the line points that make up the linear target concept
    :returns: a list of the true classifications for all points

    """
    Ax = line[0][0]
    Ay = line[0][1]
    Bx = line[1][0]
    By = line[1][1]

    classes = []

    for point in points:
        point_x = point[0]
        point_y = point[1]

        switch = np.sign( (Bx-Ax)*(point_y-Ay) - (By-Ay)*(point_x-Ax) )
        if switch >= 0:
            classes.append(1)
        else:
            classes.append(0)
    return classes




def printSet(points, classes):
    assert(len(points) == len(classes))
    print("Features;\t Class")
    for x in range(0, len(points)):
        print(points[x][0], end=" ")
        print(points[x][1], end=";\t\t ")
        print(classes[x])


noOfPoints = int(input("Please enter the number of datapoints:"))

# Used to simulate a line through lp1 and lp2 which serves as a linear target concept.
lp1_x = np.random.uniform(0, 10, 1)
lp1_y = np.random.uniform(0, 10, 1)

lp2_x = np.random.uniform(0, 10, 1)
lp2_y = np.random.uniform(0, 10, 1)

line = [(lp1_x, lp1_y), (lp2_x, lp2_y)]

points = [(int(np.random.uniform(0, 10, 1)), int(np.random.uniform(0,10,1))) for x in range(0, noOfPoints)]

classes = getClassification(points, line)
printSet(points, classes)
p = Perceptron()
p.run(points, classes)

