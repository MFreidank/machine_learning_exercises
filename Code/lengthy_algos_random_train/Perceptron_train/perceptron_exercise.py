import sys
sys.path.append("../Util/") 
from randomDataSet import *
from perceptron import *

ex_type = sys.argv[1]

noOfPoints = int(input("Please enter the number of datapoints:"))

# Used to simulate a line through lp1 and lp2 which serves as a linear target concept.
lp1_x = np.random.uniform(0, 10, 1)
lp1_y = np.random.uniform(0, 10, 1)

lp2_x = np.random.uniform(0, 10, 1)
lp2_y = np.random.uniform(0, 10, 1)

line = [(lp1_x, lp1_y), (lp2_x, lp2_y)]

points = [(int(np.random.uniform(0, 10, 1)), int(np.random.uniform(0,10,1))) for x in range(0, noOfPoints)]


classes = []
if ex_type == "lin-sep":
    classes = getLinearlySeperableClassification(points, line)
    ex_text = "Apply Perceptron Learning cyclically to the given data.\nPress enter to see the solution.\n\n"
else:
    classes = getNonLinearlySeperableClassification(points, line)
    ex_text = "The data is not linearly seperable\n.Apply Perceptron Learning cyclically to the given data until the same weight vector reappears.\n\n"
    print(output)
printSet(points, classes)

if ex_type == "lin-sep":
    input(ex_text)
    p = Perceptron()
    p.run(points, classes)
