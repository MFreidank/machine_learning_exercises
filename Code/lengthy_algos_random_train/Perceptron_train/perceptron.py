#!/usr/bin/env python

"""
Light weight Perceptron Implementation to verify results for perceptron exercises.
Traverses a set of datapoints in cyclical order
"""

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import numpy as np
import itertools as it

class Perceptron(object):
    """Main Perceptron object with bias w_0 and 2d weight vector w"""
    def __init__(self):
        super(Perceptron, self).__init__()
        # Initialize weights to zero
        self.w_0 = 0
        self.w   = [0,0]

    def classify(self, point):
        """ classify a datapoint according to the current perceptron weights
        :returns: an int representing the corresponding class
        """
        classification = np.sign(self.w_0 + self.w[0] * point[0] + self.w[1] * point[1])
        if (classification >= 0):
            return 1
        else:
            return 0

    def updateWeight(self, point, true_class):
        """ update the weights after misclassification of a datapoint 
        :point: datapoint that was misclassified
        :true_class: label of the datapoint
        """
        # If point is positive example..
        if true_class == 1:
            self.w_0 +=1
            self.w[0] += point[0]
            self.w[1] += point[1]
        elif true_class == 0:
            self.w_0 -=1
            self.w[0] -= point[0]
            self.w[1] -= point[1]
        else:
            throw("point has no valid class!")

    def existsMisclassified(self, points, classes):
        """ Checks whether points contains a point that the current perceptron misclassifies
        with respect to the labels in classes
        :points: set of datapoints to check
        :classes: set of ground truth labels of the points
        :returns: True/False depending on whether there exists a misclassified point

        """
        return False in [self.classify(points[x]) == classes[x] for x in range(0, len(points))]


    def run(self, points, classes):
        """run a perceptron cyclically over the patterns until convergence

        :points: set of datapoints
        :classes: set of labels corresponding to points

        """
        # While there exists some misclassified point do...
        while (self.existsMisclassified(points, classes)):
            for y in range(0, len(points)):
                perceptron_guess = self.classify(points[y])
                # If a point is misclassified, update the perceptron weights and continue 
                # right ahead through the patterns.
                if perceptron_guess != classes[y]:
                    self.updateWeight(points[y], classes[y])
        print("Perceptron Learning converged. Final Perceptron:")
        print("w_0:" + str(self.w_0)+ " w=" + str(self.w))
            
