#!/usr/bin/env python

"""
This script allows drawing some random DAG to train the lengthy stuff 
with respect to MLP's, namely: 
    - Forward/Backward Pass
    - Architecture Questions

"""

# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import matplotlib.pyplot as plt
import random
import networkx as nx

import sys
sys.path.append("../Util/") 
from timer import *
import numpy as np

def random_dag(nodes, edges):
    """Generate a random Directed Acyclic Graph (DAG) with a given number of nodes and edges."""
    while True:
        G = nx.DiGraph()
        for i in range(nodes):
            G.add_node(i)
        while edges > 0:
            a = random.randint(0,nodes-1)
            b=a
            while b==a:
                b = random.randint(0,nodes-1)
            G.add_edge(a,b)
            if nx.is_directed_acyclic_graph(G):
                edges -= 1
            else:
                # we closed a loop!
                G.remove_edge(a,b)
        if len([node for node in G.nodes() if G.out_degree(node) == 0]) == 1:
            break
    return G


"""Main Loop"""
mode = str(sys.argv[1])
if mode == "propagation":
    print()
elif mode == "architecture":
    print()
else:
    mode = "random"


coin = "head"
switch = np.random.uniform(0,1,1)
if switch >= 0.5:
    coin = "tail"

nodes = int(input("Please enter the number of nodes in the network:"))

G = random_dag(nodes, nodes*2)

for edge in nx.edges(G):
    random = int(np.random.uniform(1, 8, 1))
    G.edge[edge[0]][ edge[1]]['weight'] = random

pos = nx.random_layout(G)
edge_labels=dict([((u,v,),d['weight'])
             for u,v,d in G.edges(data=True)])

nx.draw(G, pos)
nx.draw_networkx_edge_labels(G,pos,edge_labels=edge_labels)

if mode == "architecture" or (mode == "random" and coin == "head"):
    print("\nExercise:\n\nWhich neurons are input/output neurons?\nHow many layers does the MLP have?\nWhich neuron belongs to which layer?\nAssume we are applying a pattern to this MLP. Give an order in which neuron activations can be calculated.\n\n\nClose the figure and press enter when you are done to see solutions.\n")
    plt.show()

    input()
    print("Input Neurons:")
    print(nx.topological_sort(G)[0])
    print("Output Neurons")
    print(nx.topological_sort(G)[len(nx.topological_sort(G))-1])
    print("Layers(Note that this is also a valid activation order):")
    print(nx.topological_sort(G))
elif mode == "propagation" or (mode == "random" and coin == "tail"):
    pattern = []
# TODO: Find out how to get all input neurons of G and then take the length to get number of inputs
    sourceNeurons = [node for node in G.nodes() if G.in_degree(node) == 0]
    noInputs = len(sourceNeurons)
    for x in range(0,noInputs):
        pattern.append(int(np.random.uniform(0,10,1)))

    targetVal = (int(np.random.uniform(0,10,1)))
    print("\nExercise:\n\nA pattern (" +str(pattern)+") with target value: " + str(targetVal)+" is applied to the MLP in the figure. Compute the error the MLP makes if all initial weights are zero. Compute the derivatives of all weights with respect to this error.\n\n\n Close the figure when you are done.")
    t = Timer()
    print("Activations:")
    for i in range(nodes):
        ran = np.random.uniform(0,1,1)
        if ran >= 0.5:
            act = "Linear"
        else:
            act = "Sigmoid"
        print(str(i) + " has " + act + " activation.")
    plt.show()
    t.getExerciseDuration()
    t.checkExerciseDuration("./fw_bw_times_"+str(nodes)+".txt")

else:
    throw("This case should never be reached!")


