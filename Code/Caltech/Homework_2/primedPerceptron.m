function [iterations, disagreement] = primedPerceptron(N, w, line, X,y);

disagreement = 0;
iterations = 0;
misclassified = [X, y];
while(rows(misclassified) > 0)
    ++iterations;
    if(rows(misclassified) > 1)
        index = round(unifrnd(1, rows(misclassified)));
    else index=1;
    endif
    yt = misclassified(index, columns(misclassified));
    xt = misclassified(index, 1:columns(misclassified)-1)';
    w = w + yt *xt;
    misclassified = [];
    for i=1:N
        xt = X(i,:)';
        yt = y(i);
        if(sign(w'*xt) != yt)
            if(rows(misclassified) == 1)
                misclassified = [xt', yt];
            else 
                misclassified = [misclassified; [xt', yt]];
            endif
        endif
    endfor
endwhile

X_test = zeros(1000,3);
y_test = zeros(1000);
for j=1:1000
    X_test(j,2)=unifrnd(-1,1);
    X_test(j,3)=unifrnd(-1,1); 
    y_test(j)=targetFunction(X_test(j,2), X_test(j,3), line);
endfor
for l=1:1000
    xt = X_test(l,:)';
    yt = y_test(l);
    if(sign(w'*xt) != yt)
        ++disagreement;
    endif
endfor
disagreement = disagreement / 1000;
end
