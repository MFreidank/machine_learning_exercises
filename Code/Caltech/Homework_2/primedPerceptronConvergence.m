function [avg_iterations] = primedPerceptronConvergence(N)

avg_iterations = 0
for i=1:100
    [E_in, w, line, X, y] = linearRegression(N); 
    [iterations, disagreement] = primedPerceptron(N, w, line, X,y);
    avg_iterations += iterations;
endfor
avg_iterations = avg_iterations/100;

end
