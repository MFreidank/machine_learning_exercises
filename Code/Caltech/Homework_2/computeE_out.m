function [E_out_avg] = computeE_out(N)

[E_in, w, line] = linearRegression(N);

E_out_avg = 0;
for i=1:10
    X = ones(1000,3);
    y = zeros(1000,1);

    % generate training samples for linear Regression
    for i=1:1000
        X(i,2)=unifrnd(-1,1);
        X(i,3)=unifrnd(-1,1); 
        y(i)=targetFunction(X(i,2), X(i,3), line);
    endfor

    E_out_avg += (1/1000)*(norm(X*w-y)*norm(X*w-y));
endfor
E_out_avg = E_out_avg/1000;

end
