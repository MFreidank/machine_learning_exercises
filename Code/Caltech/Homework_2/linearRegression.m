function [E_in, w, line, X, y] = linearRegression(N)

% two points representing the line that
% serves as target function
[lp1] =[unifrnd(-1,1), unifrnd(-1,1)];
[lp2] =[unifrnd(-1,1), unifrnd(-1,1)];
line = [lp1; lp2];

w = zeros(3,1);
X = ones(N,3);
y = zeros(N,1);

% generate training samples for linear Regression
for i=1:N
    X(i,2)=unifrnd(-1,1);
    X(i,3)=unifrnd(-1,1); 
    y(i)=targetFunction(X(i,2), X(i,3), line);
endfor

% Learning step
w = pinv(X)*y;

E_in = (1/N)*power(norm(X*w-y),2);

end
