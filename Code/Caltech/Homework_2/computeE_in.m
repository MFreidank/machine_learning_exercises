function [E_in_avg] = computeE_in(N)

E_in_avg = 0;
for i=1:1000
    E_in_avg += linearRegression(N);
endfor
E_in_avg = E_in_avg/1000;
end
