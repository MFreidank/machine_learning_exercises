function result=targetFunction(x1, x2, line)

Ax = line(1,1);
Ay = line(1,2);
Bx = line(2,1);
By = line(2,2);

result = sign( (Bx-Ax)*(x2-Ay) - (By-Ay)*(x1-Ax));
end
