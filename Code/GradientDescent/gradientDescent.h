#include <iostream>
#include <string>
#include "./gnuplot-iostream/gnuplot-iostream.h"
#ifndef GRADIENT_DESCENT_H
#define GRADIENT_DESCENT_H
class gradientDescent
{
public:
    gradientDescent (double epsilon, double mu, double initial_val, bool visualmode);
    
    double findMinimum();

    void singleUpdate();


private:
    double epsilon_;
    double delta_;
    double mu_;
    double current_value_;
    int noOfIterations_;

    inline double computeGradient()
    {
        return current_value_ * current_value_ + 100.0 * current_value_ - 100.0;
    }

    inline double getFunctionValue(double val)
    {
        return (val*val*val)/3 + 50 * val*val - 100 *val -30;
    }

    bool visualmode_;
    Gnuplot gp_;

    void plotFunctionAndPoint(double xValPoint);

};
#endif
