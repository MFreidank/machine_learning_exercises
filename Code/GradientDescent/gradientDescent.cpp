#include "gradientDescent.h"

gradientDescent::gradientDescent(double epsilon, double mu, double initial_val, bool visualmode)
{
    epsilon_ = epsilon;
    mu_ = mu;
    current_value_ = initial_val;
    delta_ = 0;
    noOfIterations_ = 0;

    eta_plus_ = eta_minus_ = 0;
    visualmode_ = visualmode;
}

double gradientDescent::findMinimum()
{
    while(computeGradient() > 0.1 || computeGradient() < -0.1)
    {
        singleUpdate();
    }
    std::cout << "Successful run of gradient descent with: epsilon = " << epsilon_ << ", mu = " <<                  mu_ << std::endl;
    std::cout << "Found the minimum of f after: " << noOfIterations_ << " iterations"<<std::endl;
    std::cout << "Minimum value is: " << current_value_ << std::endl;
    return current_value_;
}

double gradientDescent::runRprop()
{
    // Following the expert proposal in the script, 
    // we will initialize this to 0.1 here.
    delta_ = 0.1;
    delta_prime_ = delta_;
    eta_minus_ = 0.5;
    eta_plus_ = 1.2;
    gamma_ = 0;
    
    int counter = 1;
    while(computeGradient() > 0.01 || computeGradient() < -0.01)
    {
        if(counter % 2 != 0)
        {
            current_value_old_ = current_value_;
        }
        RpropUpdate();
        ++counter;
    }
    std::cout << "Successful run of Rprop with: eta_plus_ = " << eta_plus_ << ", eta_minus_ = " <<                  eta_minus_ << std::endl;
    std::cout << "Found the minimum of f after: " << counter << " iterations"<<std::endl;
    std::cout << "Minimum value is: " << current_value_ << std::endl;
    return current_value_;
}

void gradientDescent::RpropUpdate()
{
    if(visualmode_)
    {

        plotFunctionAndPoint(current_value_);
    }
    double delta_max_ = 50;
    double delta_min_ = 0.000001;
    double newGradient = computeGradient();

    if(gamma_ * newGradient > 0)
    {
        delta_prime_ = std::min(delta_prime_ * eta_plus_, delta_max_);
        delta_ = -sgn(newGradient) * delta_prime_;
        current_value_ = current_value_ + delta_ * current_value_;


        gamma_ = newGradient;
    } else if(gamma_ * newGradient < 0)
    {

        delta_prime_ = std::max(delta_prime_ *eta_minus_, delta_min_);
        current_value_ = current_value_ -  delta_ * current_value_old_;
        gamma_ = 0;
    } else if(gamma_ * newGradient == 0)
    {

        delta_ = -sgn(newGradient) * delta_prime_;
        current_value_ = current_value_ + delta_ * current_value_;

        gamma_ = newGradient;
    }
}

void gradientDescent::singleUpdate()
{
    if(visualmode_)
    {

        plotFunctionAndPoint(current_value_);
    }
    ++noOfIterations_;
    delta_ = -epsilon_ * computeGradient() + mu_ * delta_;
    current_value_ += delta_;
}


int main(int argc, char *argv[])
{
    Gnuplot gp;
    // Run three kinds of optimization algorithms iteratively to check performance.
    gradientDescent withMomentum(0.01, 0.1, 20.0, true);
    gradientDescent vanillaDescent(0.01, 0.0, 20.0,true);
    gradientDescent rprop(0.01, 0.0, 20.0, true);
    withMomentum.findMinimum();
    vanillaDescent.findMinimum();
    rprop.runRprop();
    return 0;
}

void gradientDescent::plotFunctionAndPoint(double xValPoint)
{


        std::string algorithm;
        if(eta_plus_ > 0.1)
        {
            algorithm = "Rprop";
            gp_ <<"set terminal x11 3 position -1,-210 size 800,800 enhanced font 'Verdana,10' persist\n";
        }

        if(mu_ < 0.00001 && mu_ > -0.00001)
        {
            algorithm = "Vanilla Gradient Descent";
            gp_ <<"set terminal x11 1 position -1,-210 size 800,800 enhanced font 'Verdana,10' persist\n";
        } else
        {
            algorithm = "With Momentum: mu=" + std::to_string(mu_);
            gp_ <<"set terminal x11 2 position -200,-110 size 800,800 enhanced font 'Verdana,10' persist\n";
        }
        gp_ << "set xlabel 'u'\nset ylabel 'f(u)'\n";
        gp_ << "set xrange[-60:25]\n";
        gp_ << "set yrange[-35000:55000]\n";

        gp_ << "set autoscale\n";

        gp_ << "f(x)=((x**3)/3)+50*(x**2)-100*x-30\n";
        //gp_ << "f(x)=x**2\n";
        gp_ << "x1="<<xValPoint<<"\n";
        gp_ << "plot " << "f(x) title 'f(u)' with lines linestyle 1\n";
        gp_ << "set style line 1 pointtype 7 linecolor rgb '#22aa22' pointsize 2\n";
        gp_ << "plot f(x), "
             << "'+' using ($0 == 0 ? x1 : NaN):(f(x1)):(sprintf('f(%.4f)', x1)) "
             << "with labels offset char 1,-0.2 left textcolor rgb 'blue' "
         << "point linestyle 1 title '" << algorithm.c_str() <<"'"<<"\n";
        gp_.flush();
        sleep(1);
}

