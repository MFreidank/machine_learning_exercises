function [err] = computeMeanError(y_train, X_train, y_test, X_test)

theta = linear_regress(y_train, X_train);

y_hat = linear_pred(y_test, X_test);

test = (y_hat == y_test)
err = 0;

