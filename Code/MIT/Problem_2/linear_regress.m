function [theta] = linear_regress(y,X)

theta = (inv((X'*X))*X')*y;

plot_points_and_classifier(X,y,theta);


end
