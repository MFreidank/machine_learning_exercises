function [y_hat] = linear_pred(theta, X_test)
y_hat = zeros(rows(X_test),1);
for i=1:rows(X_test)
    xt = X_test(i,:)';
    y_hat(i) = theta'*xt;
endfor
