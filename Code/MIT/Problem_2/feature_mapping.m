function [X] = feature_mapping(X_in, flag)

if(flag == 1)
    X = [ones(rows(X_in),1) X_in];
endif

if(flag == 2)
    X = [ones(rows(X_in),1) log(power(X_in,2))];
endif

if(flag > 2)
    fprintf("Second argument must be 1 or 2");
endif

end
