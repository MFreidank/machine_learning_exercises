function [theta, counter, angle] = perceptron_train(X,y)


d = columns(X);

theta = ones(d,1);


counter = 0;
i = 1;

existsMisclassified = false;
% Perceptron Learning has converged if 
% we traversed the entire matrix X
% and no patterns were misclassified.
converged = false;
while(!converged)
    xt=X(i,:)';
    yt=y(i);
    % check whether the i'th pattern is misclassified.
    if(sign(theta'*xt) != yt)
        ++counter;
        theta += yt*xt;
        existsMisclassified = true;
    endif
    % check whether all patterns were considered.
    if(i == rows(X))
        % reset the index, note this will be incremented below.
        i = 0;
        % If there exists a misclassified pattern, we have not
        % converged yet.
        converged = !existsMisclassified;
        % reset this flag.
        existsMisclassified = false;
    endif
    ++i;
endwhile

% compute angle between vector [1,0] and the perceptron
v1 = [1,0]';
angle = acosd((theta'*v1)/(norm(v1)*norm(theta)));
plot_points_and_classifier(X,y,theta);
