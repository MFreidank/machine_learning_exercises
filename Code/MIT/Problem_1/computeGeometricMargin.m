function [gamma] = computeGeometricMargin(X,y)

theta = perceptron_train(X,y);
gamma = min(abs(X*theta/norm(theta)));
end
