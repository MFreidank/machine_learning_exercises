function [gamma] = computeGeometricMargin(X,y)

theta = perceptron_train(X,y);
m = rows(X);

gamma = 0;

for i=1:m
    xt = X(i,:)';
    if(abs(((theta'*xt)/norm(theta))) > gamma)
        gamma = abs(((theta'*xt)/norm(theta)));
    endif
endfor
end
