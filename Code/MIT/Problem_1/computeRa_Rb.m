function [R_a, R_b] = computeRa_Rb()

X = load('p1_a_X.dat');
m = rows(X);

R_a = -1;
for i=1:m
    x = X(i,:);
    if(norm(x) > R_a)
        R_a = norm(x);
    endif
endfor

R_b = -1;
X = load('p1_b_X.dat');
for j=1:m
    x = X(j,:);
    if(norm(x) > R_b)
        R_b = norm(x);
    endif
endfor

