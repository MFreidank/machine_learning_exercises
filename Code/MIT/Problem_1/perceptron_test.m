function test_err = perceptron_test(theta, X_test, y_test)

test_err = 0;

n = rows(y_test)
classification = sign(theta'* X_test');

for i=1:n
    if(classification(i) != y_test(i))
        ++test_err;
    endif
    ++n;
endfor
