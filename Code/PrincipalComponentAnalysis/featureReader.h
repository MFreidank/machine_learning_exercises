#include <Eigen/Dense>
#include <string>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
using namespace std;
#ifndef FEATURE_READER
#define FEATURE_READER
class featureReader
{
public:
    featureReader (string filename);

    Eigen::MatrixXd readValues();

    inline size_t getRows() const
    {
        return rows_;
    }

    inline size_t getCols() const
    {
        return cols_;
    }

private:
    void setRowsAndCols();
    bool getNextFeatureValue(std::string& line, double& value);
    void splitString(std::string& rest, std::string& newString, char delimiter);

    std::string file_;
    size_t rows_;
    size_t cols_;
};

#endif
