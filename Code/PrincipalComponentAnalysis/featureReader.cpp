#include "featureReader.h"

featureReader::featureReader(string filename)
{
    rows_ = 0;
    cols_ = 0;
    file_ = filename;
}

Eigen::MatrixXd featureReader::readValues()
{
    setRowsAndCols();
    Eigen::MatrixXd data(rows_, cols_);

    std::ifstream file_reader;
    file_reader.open(file_);
    std::string line;

    size_t rows = 0;


    while(std::getline(file_reader, line))
    {
        ++rows;
        double current_feature;
        size_t curr_col = 0;
        size_t curr_row = rows-1;
        while(getNextFeatureValue(line, current_feature))
        {
            data(curr_row, curr_col) = current_feature;
            ++curr_col;
        }
    }
    return data;
}

void featureReader::setRowsAndCols()
{
    std::ifstream file_reader;
    file_reader.open(file_);

    std::string line;
    double current_feature;
    if(std::getline(file_reader, line))
    {
        ++rows_;
        while(getNextFeatureValue(line, current_feature))
        {
            ++cols_;
        }
    }

    while(std::getline(file_reader, line))
    {
        ++rows_;
    }
}

bool featureReader::getNextFeatureValue(std::string& line, double& value)
{
    std::string featureString;
    
    splitString(line, featureString, ',');
    if(isdigit(featureString[0]))
    {
        // convert to double and store in value
        istringstream i(featureString);
        if(!(i >> value))
        {
           std::cerr << "Invalid value conversion to double" << std::endl;
           return false;
        }
        return true;
    } else 
    {
        return false;
    }
}

void featureReader::splitString(std::string& rest, std::string& newString, char delimiter)
{
    size_t index = 0;
    for (size_t i = 0; i < rest.size(); ++i)
    {
        if(rest[i] != delimiter)
        {
            newString += rest[i];
            index = i;
        } else 
        {
            break;
        }
    }
    if(index+2 >= rest.size())
    {
        return;
    }
    rest = rest.substr(index+2, rest.size());
}
