#include "featureReader.h"
#include <Eigen/Eigen>
using namespace std;
using namespace Eigen;
#ifndef PCA_H
#define PCA_H
class pca
{
public:
    pca (std::string filename, size_t k);

    void performPreProcessing();

private:

    void meanNormalization();
    void featureScaling();
    void computeFeatureMeans();

    void computeCovarianceMatrix();
    void diagonalize();


    Eigen::MatrixXd data_;
    Eigen::MatrixXd U_;
    Eigen::MatrixXd S_;
    Eigen::MatrixXd cov_;
    Eigen::VectorXd featureMeans_;

    size_t rows_;
    size_t cols_;

    size_t k_;

};


#endif
