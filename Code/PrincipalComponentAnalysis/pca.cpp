#include "pca.h"


pca::pca(std::string filename, size_t k)
{
    k_ = k;
    featureReader reader(filename);
    data_ = reader.readValues();
    rows_ = reader.getRows();
    rows_ = reader.getCols();
    performPreProcessing();
    computeCovarianceMatrix();
}

void pca::performPreProcessing()
{
    meanNormalization();
    featureScaling();
}

void pca::meanNormalization()
{
    computeFeatureMeans();
    for (int i = 0; i < data_.rows(); ++i)
    {
        for (int j = 0; j < data_.cols(); ++j)
        {
            data_(i,j) -= featureMeans_[j];
        }
    }
}

void pca::featureScaling()
{
    double min = data_.minCoeff();
    double max = data_.maxCoeff();

    for (int i = 0; i < data_.rows(); ++i)
    {
        for (int j = 0; j < data_.cols(); ++j)
        {
            data_(i,j) = (data_(i,j) - min)/(max-min);
        }
    }
}

void pca::computeCovarianceMatrix()
{
    cov_.resize(data_.cols(), data_.cols());
    cov_ = data_.transpose() * data_;
    cov_ *= (1.0/(data_.rows()-1.0));
    diagonalize();
}

void pca::diagonalize()
{
    JacobiSVD<MatrixXd> svd(cov_, ComputeThinU);
    auto eigenvalues = svd.singularValues();
    S_ = eigenvalues;
    U_ = svd.matrixU(); 

    // TODO: Detect largest eigenvalues by sorting, instead of harcoding
    Eigen::MatrixXd U_reduced(U_.rows(),k_);
    U_reduced.col(0) = U_.col(0);
    U_reduced.col(1) = U_.col(1);
    Eigen::MatrixXd Z(data_.rows(), data_.cols());

    Z = (U_reduced.transpose() * data_.transpose()).transpose();

    std::cout << "Reduced Dataset:" << std::endl;
    std::cout << Z << std::endl;



}

int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        std::cout << "Usage: ./pca k:size_t" << std::endl;
        return -1;
    }
    std::string s = argv[1];
    istringstream ss(s);
    size_t k; 
    ss >> k;
    pca p("./data/iris.data", k);
    
    return 0;
}

void pca::computeFeatureMeans()
{
    featureMeans_.resize(data_.cols());
    for (int i = 0; i < data_.rows(); ++i)
    {
        for (int j = 0; j < data_.cols(); ++j)
        {
            featureMeans_(j) += data_(i,j);
        }
    }
    featureMeans_ *= (1.0/data_.rows());
}
