function [error_test] = testError(X_test, y_test, theta)
    error_test = linearRegCostFunction(X_test, y_test, theta, 0);
end
