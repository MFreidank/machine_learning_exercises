function [J, grad] = linearRegCostFunction(X, y, theta, lambda)
%LINEARREGCOSTFUNCTION Compute cost and gradient for regularized linear 
%regression with multiple variables
%   [J, grad] = LINEARREGCOSTFUNCTION(X, y, theta, lambda) computes the 
%   cost of using theta as the parameter for linear regression to fit the 
%   data points in X and y. Returns the cost in J and the gradient in grad

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost and gradient of regularized linear 
%               regression for a particular choice of theta.
%
%               You should set J to the cost and grad to the gradient.
%

grad = grad(:);
for i=1:m
    firstpart = theta'*X(i,:)';
    term = firstpart - y(i);
    J += term * term;
endfor
J *= (1/(2*m));

reg = 0;
for j=2:size(theta,1)
    reg += theta(j) * theta(j);
endfor
reg *= (lambda/(2*m)); 

J += reg;
    


for j=1:size(theta,1)
    for i=1:m
        grad(j) += (1/m)*((theta'*X(i,:)') - y(i))*X(i,j); 
    endfor
    if(j > 1)
        grad(j) += (lambda/m) * theta(j);
    endif
endfor

% =========================================================================



end
