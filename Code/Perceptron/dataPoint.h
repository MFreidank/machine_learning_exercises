#ifndef DATAPOINT_H
#define DATAPOINT_H 
class dataPoint
{
public:
    dataPoint ();
    inline bool isPositive() const
    {
      return positive;
    }

    inline double values(int i) const
    {
      return values_(i);
    }
    void setValues(Eigen::VectorXd values);

private:
    bool positive;
    Eigen::VectorXd values_;



};
#endif
