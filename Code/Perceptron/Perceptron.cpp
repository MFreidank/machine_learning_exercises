#include "Perceptron.h"

using weights = Eigen::VectorXd;
Perceptron::Perceptron(int dimension)
{
    weights_ = Eigen::VectorXd::Random(dimension);
}

Perceptron::Perceptron(weights W)
{
    weights_ = W;
}

void Perceptron::updateWeights(dataPoint example)
{
    if(example.isPositive())
    {
        ++weights_(0);
        for (int i = 1; i < weights_.size(); ++i)
        {
          weights_(i) += example.values(i);
        }
    } else 
    {
        --weights_(0);
        for (int i = 1; i < weights_.size(); ++i)
        {
          weights_(i) -= example.values(i);
        }
    }
}

double Perceptron::classify(dataPoint point)
{
  double result = weights_(0);
  for (int i = 1; i < weights_.size(); ++i)
  {
    result += weights_(i) * point.values(i);
  }
  return result;
}

int Perceptron::findNextMisclassified(std::vector<dataPoint> examples)
{
  for (int i = 0; i < examples.size(); ++i)
  {
    if(examples(i).isPositive() && (classify(examples(i)) < 0))
    {
      return i;
    }
    if(examples(i).isNegative() && (classify(examples(i)) >= 0))
    {
      return i;
    }
  }
    return -1;
}


// Run of perceptron until convergence (if possible).
bool Perceptron::run(std::vector<dataPoint> examples)
{
    // Check that the dimensions of an example and the weights add up.
    assert(weights_.size() == examples.size()+1);

    while(true)
    {
        int nextMisclassified = findNextMisclassified(examples);
        if(nextMisclassified == -1)
        {
            // No points misclassified. Converged correctly.
            return true;
        } else 
        {
          dataPoint misclassifiedPoint = examples(nextMisclassified);
          updateWeights(misclassifiedPoint);
        }
    }
}


int main(int argc, char *argv[])
{
    
    return 0;
}
