#include "dataPoint.h"
#include <assert.h>

dataPoint::dataPoint(int dimension, bool positive)
{
    positive_ = positive;
    dimension_ = dimension;
    values_.resize(dimension_);
}

void dataPoint::setValues(Eigen::VectorXd values)
{
    assert(dimension_ == values.size());
    values_ = values;
}
