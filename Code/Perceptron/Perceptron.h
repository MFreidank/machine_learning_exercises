#include <Eigen/Dense>
#include <assert.h>
#include <vector>
#include "Util.h"
#include "dataPoint.h"


#ifndef PERCEPTRON_H
#define PERCEPTRON_H 
class Perceptron
{
public:
    Perceptron(int dimension);
    Perceptron(Eigen::VectorXd Weights);

    void updateWeights(dataPoint example);
    int findNextMisclassified(std::vector<dataPoint> examples);
    double classify(dataPoint point);
    bool run(std::vector<dataPoint> examples);

private:
    Eigen::VectorXd weights_;
};
#endif
