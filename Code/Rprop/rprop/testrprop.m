function testrprop()
    
    figure(1,'name', 'f(u) = u^3 + 50 u^2 - 100 u - 30 ');
    graphics_toolkit('gnuplot');
    [x, fx, i] = rprop(20, @exerciseSheet03, 100);
    fprintf('Rprop: minimum found at x=%f after %i function evaluations\n', x, i)
    plot(0:i, fx, "-b;Rprop;")
    title('Minimization algorithm comparison: f(u) = u^3 + 50 u^2 - 100 u - 30 ')
    xlabel('number of function evaluations')
    ylabel('objective function value')
    hold on;

    [x, fx, i] = vanillaDescent(20, @exerciseSheet03, 100);
    fprintf('Vanilla Descent: minimum found at x=%f after %i function evaluations\n', x, i)
    
    plot(0:i, fx, "color", 'r',"-b;Vanilla Descent;")
    xlabel('number of function evaluations')
    ylabel('objective function value')
    hold on;

    [x, fx, i] = momentumDescent(20, @exerciseSheet03, 100); 
    fprintf('Gradient Descent with Momentum: minimum found at x=%f after %i function evaluations\n', x, i)
    plot(0:i, fx, "color", 'g', "-b;With Momentum;")
    xlabel('number of function evaluations')
    ylabel('objective function value')
    hold off;

    figure(2,'name', 'f(x) = 2*x^2 - x +1');
    graphics_toolkit('gnuplot');
    [x, fx, i] = rprop(2, @parabola, 100, 2, -1, 1);
    fprintf('Rprop: minimum found at x=%f after %i function evaluations\n', x, i)
    plot(0:i, fx, "-b;Rprop;")
    title('Minimization algorithm comparison: f(x) = 2*x^2 - x +1');
    xlabel('number of function evaluations')
    ylabel('objective function value')
    hold on;

    [x, fx, i] = vanillaDescent(2, @parabola, 1000, 2, -1, 1);
    fprintf('Vanilla Descent: minimum found at x=%f after %i function evaluations\n', x, i)
    
    plot(0:i, fx, "color", 'r',"-b;Vanilla Descent;")
    xlabel('number of function evaluations')
    ylabel('objective function value')
    hold on;

    [x, fx, i] = momentumDescent(2, @parabola, 1000, 2, -1, 1);
    fprintf('Gradient Descent with Momentum: minimum found at x=%f after %i function evaluations\n', x, i)
    plot(0:i, fx, "color", 'g', "-b;With Momentum;")
    xlabel('number of function evaluations')
    ylabel('objective function value')
    hold off;

    

end

function [y, g] = parabola(x, a, b, c)

    y = a*x^2 + b*x + c;
    g = 2*a*x + b;

end

function [y,g] = exerciseSheet03(u)
  
  y = ((u^3)/3) + 50*u^2 - 100*u - 30;
  g = u^2 +100*u -100;
end
