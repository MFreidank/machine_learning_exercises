function [x, fx, i] = vanillaDescent(x, func, m, varargin)

% Minimize a differentiable multivariate function using Vanilla Gradient Descent.
%
% Usage: [X, fX, i] = vanillaDescent(x, func, m, P1, P2, P3, ... )
% 
% x       Initial guess.
% func    The name or pointer to the function to be minimized. The function
%         func must return two arguments, the value of the function, and it's
%         partial derivatives wrt the elements of x. The partial derivative  
%         must have the same type as x.
% m       Length of the run; the maximum allowed number of function evaluations.
% P1, P2, ... parameters are passed to the function func.
%
% x       The returned solution.
% fx      Vector of function values indicating progress made.
% i       Number of iterations.

% algorithms stops if gradient norm is smaller than eps
eps_stop = 0.01;

% we allow negative m to be consistent with Rasmussen's minimize.m
m = abs(m);

% default parameters
epsilon = 0.01


% variable initializations
fx = [];

% initial function evaluation
[f, df] = feval(func, x, varargin{:});
fx = [fx' f]'; 

for i=1:m
    
    % single gradient descent update
    x = x - epsilon*df;
    
    % function evaluation
    [f, df] = feval(func, x, varargin{:});
    fx = [fx' f]'; 

    % check gradient norm 
    if norm(df) < eps_stop, break, end
    
end

end



