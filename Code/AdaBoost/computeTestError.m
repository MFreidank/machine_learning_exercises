function [test_error] = computeTestError(weights, line, titleString)
[X_test, y_test] = generateDataset(1000, line);
test_error = 0;
for i=1:rows(X_test)
    x_t = X_test(i,:)';
    y_t = y_test(i);
    if(sign(weights*x_t) != y_t)
        ++test_error;
    endif
endfor
test_error /= rows(X_test)
plotPerceptronAndData(X_test,y_test,weights, titleString);

end
