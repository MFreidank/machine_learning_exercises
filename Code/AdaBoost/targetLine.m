function [line] = targetLine()

point1(1,1) = unifrnd(-1,1);
point1(1,2) = unifrnd(-1,1);

point2(1,1) = unifrnd(-1,1);
point2(1,2) = unifrnd(-1,1);

line = [point1; point2];

end
