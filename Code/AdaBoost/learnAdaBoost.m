function [classifiers, classifierweights] = learnAdaBoost(T, numberOfTrainingSamples)
% TODO: Make more generic: Let this take a function argument to allow 
% using different classification algorithms for boosting
classifiers = zeros(T, 3);
classifierweights = zeros(T,1);


% Generate training dataset and initialize target line
[X,y,line] = generateDataset(numberOfTrainingSamples, []);
% Initialize distribution for sampling.
D = (1/numberOfTrainingSamples) * (ones(1, numberOfTrainingSamples));
% Initialize distribution
for t=1:T
    t
    % Generate new dataset by subsampling
    [X,y] = subsampleData(X,y,D);
    weights = perceptronLearner(X,y)';
    classifiers(t,:) = weights;
    e_t = expectedError(X,y,weights,D)
    classifierweights(t) = (1/2) * log(((1-e_t)/e_t));
    D = recomputePatternDistribution(X,y,classifierweights(t), D, weights);
endfor

finalH = classifierweights'*classifiers;

plotPerceptronAndData(X,y,classifiers(1,:), "Train: No Boosting");
plotPerceptronAndData(X,y,finalH, "Train: Boosting");

test_error_boosting = computeTestError(finalH, line, "Test: Boosting")
test_error_bestSinglePerceptron = computeTestError(classifiers(1,:), line, "Test: No Boosting")




end
