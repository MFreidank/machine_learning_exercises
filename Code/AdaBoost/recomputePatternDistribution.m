function [D_new] = recomputePatternDistribution(X,y,alpha, D, weights)
D_new = D;

% Set normalization factor Z
Z = 0;
for j=1:columns(D)
    x_t = X(j,:)';
    Z += D(1,j)*exp(-alpha*y(j)*sign(weights*x_t));
endfor
for i=1:columns(D)
    x_t = X(i,:)';
    D_new(1,i) = (D(1,i)/Z) * exp(-alpha*(y(i)*sign(weights*x_t)));
endfor

end
