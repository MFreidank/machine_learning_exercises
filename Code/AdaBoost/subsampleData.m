function [X,y] = subsampleData(X_in, y_in, D)

X = zeros(size(X_in));
y = zeros(size(y_in));

for j=1:columns(D)
    u = unifrnd(0.00001, 1);
    t = 0;
    for i=1:columns(D)
        t += D(1,i);
        if(t > u)
            X(j,:) = X_in(i,:);
            y(j) = y_in(i);
            break;
        endif
    endfor
endfor
end
