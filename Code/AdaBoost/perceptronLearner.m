function [weights] = perceptronLearner(X_train,y_train)

weights = zeros(3,1);
numberOfPatterns = rows(X_train);
converged = false;


iterationcounter = 0;
while(!converged && iterationcounter < 300)
    for i=1:numberOfPatterns
        if(i == 1)
            converged = true;
        endif
        x_i = X_train(i,:)';
        y_i = y_train(i);
        if(sign(weights'*x_i) != y_i)
            ++iterationcounter;
            converged = false;
            weights += y_i * x_i;
        endif
    endfor
endwhile



end
