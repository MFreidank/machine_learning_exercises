function [targetValue] = evaluateTargetFunction(targetline, trainingPattern)

x1 = trainingPattern(1,1);
x2 = trainingPattern(1,2);

Ax = targetline(1,1);
Ay = targetline(1,2);
Bx = targetline(2,1);
By = targetline(2,2);

targetValue = sign( (Bx-Ax)*(x2-Ay) - (By-Ay)*(x1-Ax));

end
