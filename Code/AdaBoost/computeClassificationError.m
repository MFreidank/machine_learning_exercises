function error_term = computeClassificationError(X,y,weights)
error_term = 0;
for i=1:rows(X)
    x_t = X(i,:)';
    y_t = y(i);
    if(sign(weights*x_t) != y_t)
        ++error_term;
    endif
endfor
error_term /= rows(X)
end
