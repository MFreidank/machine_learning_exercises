function [error_term] = expectedError(X,y,weights,D)

error_term = 0;
for i=1:rows(X)
    x_t = X(i,:)';
    y_t = y(i);
    I = 0;
    if(sign(weights*x_t)!= y_t)
        I = 1;
    endif
    
    error_term += D(i) * I;

endfor

end
