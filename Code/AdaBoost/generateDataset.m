function [X,y,line] = generateDataset(numberOfPatterns, line)

% Initialize matrix for the patterns and targetvector
X = [ones(numberOfPatterns,1) zeros(numberOfPatterns, 2)];
y = zeros(numberOfPatterns,1);

% If we did not specify a targetFunction, generate a new one.
if(size(line) == 0)
    line = targetLine();
endif


% Fill the matrix and targetVector with values
for i=1:numberOfPatterns
    X(i,2) = unifrnd(-1,1);
    X(i,3) = unifrnd(-1,1);
    x = [X(i,2) X(i,3)];
    y(i) = evaluateTargetFunction(line, x);
endfor
% Jitter some noise onto the data, rendering it not linearly seperable and allowing 
% adaboost to show its muscles
for i=1:(numberOfPatterns/50)
    rand_index = round(unifrnd(1, numberOfPatterns));
    % Flip the sign of a random sample picked sample
    y(rand_index) *= -1;
endfor


end
